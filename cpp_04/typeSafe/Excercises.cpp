#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <stdexcept>
#include <string>
#include <any>
#include <variant>
#include <optional>

using namespace std::string_literals;

namespace variant
{
    union Union
    {
        ~Union() {}
        int i;
        std::string s;
    };

    TEST_CASE("union tests")
    {
        SECTION("will return int")
        {
            Union u{1};
            REQUIRE(u.i == 1);
        }
        
        SECTION("will return string")
        {
            Union u{};
            new (&u.s) std::string("abc");
            REQUIRE(u.s == "abc"s);
            u.s.~basic_string();
        }
    }

    // using Variant = /* ? */;

    // template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
    // template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

    // TEST_CASE("variant tests")
    // {
    //     SECTION("will return int")
    //     {
    //         Variant v{1};
    //         REQUIRE(/* ? */ == 1); // get stored type
    //         REQUIRE_THROWS_AS(/* ? */, std::bad_variant_access); // try to get other type
    //         REQUIRE(/* ? */ == 1); // get if stored type
    //         REQUIRE(/* ? */ == nullptr); // get other type
    //     }
        
    //     SECTION("will return string")
    //     {
    //         Variant v{"abc"s};
    //         REQUIRE(/* ? */ == "abc"s); // get stored type
    //         REQUIRE_THROWS_AS(/* ? */, std::bad_variant_access); // try to get other type
    //         REQUIRE(/* ? */ == "abc"s); // get if stored type
    //         REQUIRE(/* ? */ == nullptr); // get other type
    //     }

    //     SECTION("visit")
    //     {
    //         Variant variantHoldingInt{1};
    //         Variant variantHoldingString{"abc"s};
    //         auto o = overloaded {
    //             /* ? */, // lambda printing info about int
    //             /* ? */ // lambda printing info about string
    //         };
    //         std::visit(o, variantHoldingString);
    //         std::visit(o, variantHoldingInt);
    //     }
    // }

}

namespace optional
{
    void* oldStyleFunction(bool initialized)
    {
        if (initialized)
            return static_cast<void*>(new int{1});
        else 
            return nullptr;
        throw std::runtime_error("Bad type flow");
    }

    // /* ? */ newStyleFunction(bool initialized)
    // {
    //     /* ? */
    // }

    TEST_CASE("void* optional tests")
    {
        SECTION("if initialized, will return value")
        {
            auto* i = static_cast<int*>(oldStyleFunction(true));
            REQUIRE(*i == 1);
            delete i;
        }

        SECTION("if not initialized, will return nullptr")
        {
            auto* i = oldStyleFunction(false);
            REQUIRE(i == nullptr);
        }
    }

    // TEST_CASE("optional tests")
    // {
    //     SECTION("if initialized, will return value")
    //     {
    //         std::optional o = newStyleFunction(true);
    //         REQUIRE(/* ? */); // check if o has a value
    //         REQUIRE(/* ? */ == 1); // check value of o
    //     }

    //     SECTION("if not initialized, will be empty")
    //     {
    //         std::optional o = newStyleFunction(false);
    //         REQUIRE(/* ? */); // check if o has a value
    //         REQUIRE_THROWS_AS(/* ? */, std::bad_optional_access); // check if o throws
    //         REQUIRE(/* ? */ == -1); // if no value, get -1
    //     }
    // }
}

namespace any
{
    template<typename T>
    void* oldStyleFunction(const T& t)
    {
        if (typeid(t) == typeid(int))
            return static_cast<void*>(new int{2});
        else if (typeid(t) == typeid(std::string))
            return static_cast<void*>(new std::string{"abc"});
        throw std::runtime_error("Bad type flow");
    }

    // /* ? */ newStyleFunction(const /* ? */& t)
    // {
    //     /* ? */
    // }

    TEST_CASE("void* tests")
    {
        SECTION("will return int")
        {
            auto i = static_cast<int*>(oldStyleFunction(2));
            REQUIRE(*i == 2);
            delete i;
        }

        SECTION("will return string")
        {
            auto* s = static_cast<std::string*>(oldStyleFunction("abc"s));
            REQUIRE(*s == "abc"s);
            delete s;
        }

        SECTION("will throw when different type given")
        {
            REQUIRE_THROWS_AS(oldStyleFunction(2.5), std::runtime_error);
        }
    }

    // TEST_CASE("any tests")
    // {
    //     SECTION("will return int")
    //     {
    //         std::any a = newStyleFunction(1);
    //         REQUIRE(/* ? */); // check if a has a value
    //         REQUIRE(/* ? */ == typeid(int)); // check type of a
    //         REQUIRE(/* ? */ == 2); // check value of a
    //     }

    //     SECTION("will return string")
    //     {
    //         std::any a = newStyleFunction("abc"s);
    //         REQUIRE(/* ? */); // check if a has a value
    //         REQUIRE(/* ? */ == typeid(std::string)); // check type of a
    //         REQUIRE(/* ? */ == "abc"s); // check value of a
    //     }

    //     SECTION("will return empty any")
    //     {
    //         std::any a = newStyleFunction(2.5);
    //         REQUIRE(/* ? */); // check if a has a value
    //         REQUIRE_THROWS_AS(/* ? */, std::bad_any_cast); // check  if throws when getting the value
    //     }
    // }
}