#include <cstdio>
#include <initializer_list>
#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <algorithm>
#include <cmath>
#include <functional>
#include <iostream>
#include <iterator>
#include <numeric>
#include <random>
#include <array>
#include <vector>

struct DemoStruct
{
    DemoStruct(int f1, double f2) : field(f1), field2(f2) {}
    DemoStruct(DemoStruct&) = default;
    void printField() const { std::cout << "field: " << field << std::endl; }
    void increment() { ++field; }
    int field;
    double field2;
};

template<typename P>
class DemoPointer
{
public:
    DemoPointer(P* ds) : demoStruct(ds) {}
    ~DemoPointer() { delete demoStruct; }
    P& operator*() { return *demoStruct; }
    P operator*() const { return *demoStruct; }
    P* operator->() { return demoStruct; }
    P* get() { return demoStruct; }
private:
    P* demoStruct;
};

template<typename T, typename ...Args>
DemoPointer<T> makeDemoPointer(Args... args)
{
    return { new T(std::forward<Args>(args)...) };
}

void function1(DemoStruct* ds)
{
    ds->increment();
}

void function2(const DemoStruct* ds)
{
    ds->printField();
}

TEST_CASE("pointer exercises")
{
    SECTION("")
    {
        auto p = makeDemoPointer<DemoStruct>(1, 2.4);
        std::cout << p->field << std::endl;
        std::cout << p->field2 << std::endl;
        p->increment();
        std::cout << p->field << std::endl;
        p->printField();
        function1(p.get());
        function2(p.get());  
        auto& ref = *p;
        ref.printField();
    }
}