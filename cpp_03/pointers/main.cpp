#include <memory>
#include <iostream>

struct Foo
{
    Foo(std::unique_ptr<int>&& i) : i(i) { std::cout << "moving constructor 2" << std::endl; }
    Foo(const Foo&) = delete;
    const std::unique_ptr<int>& i;
};

int main()
{
    std::unique_ptr<int> up;// = std::make_unique<int>(2); // std::unique_ptr<int> up{new int{1}};
    std::shared_ptr<int> sp = std::make_shared<int>(2);
    std::weak_ptr<int> wp;
    
    Foo f(std::move(up));
}