#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <iostream>
#include <string>

class DemoStruct
{
public:
    DemoStruct(int i, double d) : foo(i), bar(d) {}
    DemoStruct(const DemoStruct&) = default;
    ~DemoStruct() { std::cout << "destructor" << std::endl; }
    int getFoo() const noexcept { return foo; }
    void increment() { ++foo; }

    double bar;
private:
    int foo;
};

void printFoo(const DemoStruct* ds)
{
    std::cout << ds->getFoo() << std::endl;
}

void incrementFoo(DemoStruct* ds)
{
    ds->increment();
}

//----------------------------------------------------------------------------

template<typename P>
class Pointer
{
public:
    Pointer(P* ds) : pointee(ds) {}
    ~Pointer() { delete pointee; }
    P* operator->() { return pointee; }
    operator P*() { return pointee; }
    P* get() { return pointee; }
    P& operator*() { return *pointee; }
    P operator*() const { return *pointee; }
private:
    P* pointee;
};

template<typename T, typename ... Args>
Pointer<T> makePointer(Args... args)
{
    return Pointer<T>(new T{std::forward<Args>(args)...});
}

struct SecondDemoStruct
{
    SecondDemoStruct(const std::string& s, int i, double d) : s(s), i(i), d(d) {}

    std::string s;
    int i;
    double d;
};

TEST_CASE("Pointers demo")
{
    SECTION("Create test")
    {
        auto p = makePointer<SecondDemoStruct>("abc", 3, 3.5);
        REQUIRE(p->s == "abc");
        REQUIRE(p->i == 3);
        REQUIRE(p->d == 3.5);
    }

    SECTION("Create test")
    {
        auto p = makePointer<DemoStruct>(1, 2.5);
        REQUIRE(p->bar == 2.5);
        REQUIRE(p->getFoo() == 1);
    }

    SECTION("Function with pointer argument test")
    {
        auto p = makePointer<DemoStruct>(1, 2.5);
        incrementFoo(p);
        REQUIRE(p->getFoo() == 2);
    }

    SECTION("Pointer::get() Function with pointer argument test")
    {
        auto p = makePointer<DemoStruct>(1, 2.5);
        incrementFoo(p.get());
        REQUIRE(p->getFoo() == 2);
    }

    SECTION("Function with pointer argument test")
    {
        auto p = makePointer<DemoStruct>(1, 2.5);
        printFoo(p);
        REQUIRE(p->getFoo() == 1);
        REQUIRE(p->bar == 2.5);
    }

    SECTION("Pointer::get() Function with pointer argument test")
    {
        auto p = makePointer<DemoStruct>(1, 2.5);
        printFoo(p.get());
        REQUIRE(p->getFoo() == 1);
        REQUIRE(p->bar == 2.5);
    }

    SECTION("Pointer::get() Function with pointer argument test")
    {
        auto p = makePointer<DemoStruct>(1, 2.5);
        auto& ref = *p;
        REQUIRE(ref.getFoo() == 1);
        REQUIRE(ref.bar == 2.5);
    }
}