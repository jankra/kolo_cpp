#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <algorithm>
#include <cmath>
#include <functional>
#include <iostream>
#include <iterator>
#include <numeric>
#include <random>
#include <array>
#include <vector>

TEST_CASE("lambda exercises")
{
    using namespace Catch::Matchers;
    std::vector<int> data{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    SECTION("count even numbers")
    {
		auto evenNumbersCount = std::count_if(begin(data), end(data), /* ? */);
        REQUIRE(evenNumbersCount == 5);
    }

	SECTION("copy even numbers")
	{
		std::vector<int> evenNumbers;
		std::copy_if(begin(data), end(data), back_inserter(evenNumbers), /* ? */);
        REQUIRE_THAT(evenNumbers, Equals(std::vector<int>{2, 4, 6, 8, 10}));
    }

    SECTION("create vector of squares")
    {
        std::vector<int> squares(data.size());
		std::transform(begin(data), end(data), begin(squares), /* ? */);
        REQUIRE_THAT(squares, Equals(std::vector<int>{1, 4, 9, 16, 25, 36, 49, 64, 81, 100}));
    }

    SECTION("remove from container items divisible by any number from a given array")
    {
        const std::array<int, 4> eliminators = {3, 4, 5, 7};
        auto dataCopy = data;
        // use std::remove_if()
        // use std::any_of()
        REQUIRE_THAT(dataCopy, Equals(std::vector<int>{1, 2}));
    }
}