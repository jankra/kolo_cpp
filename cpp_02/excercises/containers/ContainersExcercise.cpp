#define CATCH_CONFIG_MAIN

#include <string>
#include <vector>
#include <map>
#include <stdexcept>
#include "catch.hpp"

using namespace std::string_literals;

#define ERROR_MESSAGE ("PhoneBook::"s + __FUNCTION__ + "() not yet Implemented!"s)

class PhoneBook
{
public:
    unsigned size() const
    {
        // ?
    }

    void append(int number, const std::vector<std::string>& namesToAdd)
    {
        // ?
    }

    std::vector<std::string> getNames(int number)
    {
        // ?
    }

private:
    // ?
};


TEST_CASE("c-style mapper tests")
{
    SECTION("empty mapper has size equal to 0")
    {
        PhoneBook pb{};
        REQUIRE(pb.size() == 0);
    }

    SECTION("size should return records size")
    {
        PhoneBook pb{};
        pb.append(111'111'111, { "Name1"s });
        pb.append(222'222'222, { "Name2"s });
        REQUIRE(pb.size() == 2);
    }
 
    SECTION("should return names corresponding to given number")
    {
        PhoneBook pb{};
        int firstNumber = 111'111'111;
        std::vector<std::string> firstNameSet{"Name1"s};
        int secondNumber = 222'222'222;
        std::vector<std::string> secondNameSet{ "Name2"s, "Name3"s};
        pb.append(firstNumber, firstNameSet);
        pb.append(secondNumber, secondNameSet);
        REQUIRE(pb.getNames(firstNumber) == firstNameSet);
        REQUIRE(pb.getNames(secondNumber) == secondNameSet);
    }
}