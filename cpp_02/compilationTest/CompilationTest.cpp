#define CATCH_CONFIG_MAIN

#include <string>
#include "catch.hpp"

using namespace std::string_literals;

TEST_CASE("compilation test")
{
    SECTION("hello world")
    {
        auto testText = "Hello World"s;
        REQUIRE(testText == "Hello World"s);
    }

}