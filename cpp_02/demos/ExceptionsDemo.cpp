#include <iostream>
#include <exception>

namespace exceptions
{

using VeryBadError = std::runtime_error;

void errorProneFunction(bool somethingBadHappens)
{
    if (not somethingBadHappens)
    {
        std::cout << __FUNCTION__ << ": Evertything ok!" << std::endl;
    }
    else 
    {
        auto errorMessage = std::string(__FUNCTION__) + ": Something bad happened :(";
        throw VeryBadError(errorMessage);
    }
}

void runDemo()
{
    std::cout << "Success flow:" << std::endl;
    try 
    {
        errorProneFunction(false);
    }
    catch (const std::exception& e)
    {
        std::cout << "Exception caught: " << e.what() << std::endl;
    }

    std::cout << std::endl << "Failed flow:" << std::endl;
    
    try 
    {
        errorProneFunction(true);
    }
    catch (const std::exception& e)
    {
        std::cout << "Exception caught: " << e.what() << std::endl;
    }
}
}

int main()
{
    exceptions::runDemo();
}