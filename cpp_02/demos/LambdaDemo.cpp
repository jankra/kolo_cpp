#include <functional>
#include <sstream>
#include <stdexcept>
#include <tuple>
#include <iostream>
#include <exception>
#include <vector>
#include <map>
#include <algorithm>
#include <functional>

using namespace std::string_literals;

namespace lambda
{

template <typename T>
void log(const T& msg)
{
    std::cout << msg << std::endl;
}

struct Functor
{
    Functor() = default;
    Functor(const Functor& f)
    {
        log("Functor copy constructor called!");
    };
    void operator()(int a) const
    {
        log("Functor called: " + std::to_string(a));
    }
};

using DemoLambda = std::function<void()>;

void runDemo(const std::string& name, const DemoLambda& demo)
{
    log(name + " demo start");
    demo();
    std::cout << std::endl;
}

void runDemo()
{
    auto runSimpleLambdaDemo = []
    {
        auto simplestLambdaThatDoesNothing = []{};
        simplestLambdaThatDoesNothing();
        
        int anInt = 5;
        auto lambdaWithAutoArguments = [] (auto arg) { log(arg); }; // auto as an argument valid since C++14
        lambdaWithAutoArguments(anInt);

        auto lambdaWithSpecifiedType = [] () -> int { return 2; }; // if all returns return the same type, no specified return type is ok
        log(lambdaWithSpecifiedType());
    };

    auto runAlgorithmUsageExamples = []
    {
        std::vector<int> v = {1, 2, 3, 4};
        auto sortFromHighestToLowest = [](auto lhs, auto rhs) { return rhs < lhs; };
        std::sort(v.begin(), v.end(), sortFromHighestToLowest);

        auto logElement = [](auto e) { log(e); };
        std::for_each(v.begin(), v.end(), logElement);
    };

    auto runCatchBlockEamples = []
    {
        auto lambda = [] (auto a){ log("Lambda call: " + std::to_string(a)); }; // lambda might be of a compiler created functor type 
                                                                               // or a function pointer - not specified by the standard
        Functor funct;

        using Caster =  std::function<void(int)>;

        Caster catchingAllByRef = 
            [&](int a)  { 
                lambda(a);
                funct(a);
            };
        catchingAllByRef(5);

        Caster catchingAllByCopy = 
            [=](int a)  { 
                lambda(a);
                funct(a);
            };
        catchingAllByRef(5);
    };
}

}

int main()
{
    lambda::runDemo();
}