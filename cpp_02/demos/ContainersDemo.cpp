#include <sstream>
#include <tuple>
#include <iostream>
#include <exception>
#include <vector>
#include <map>
#include <algorithm>

using namespace std::string_literals;

namespace containers
{

template <typename T>
void log(const T& msg)
{
    std::cout << msg << std::endl;
}

void runPairDemo()
{
    log(std::string(__FUNCTION__) + "(): Demo start");
    auto p = std::make_pair<int, double>(1, 2.5);
    log(p.first);
    log(p.second);
    log(std::get<0>(p));
    log(std::get<double>(p));
}

void runArrayDemo()
{
    log(std::string(__FUNCTION__) + "(): Demo start");
    log("Following code is valid for most containers: ");
    
    std::array<int, 3> array{1, 2, 3}; // substitute for: int array[3] = {1, 2, 3};
    log(array.size());
    log(std::get<2>(array));
    log(array[2]);
    log(array.at(2));
    try
    {
        // log(array[4]); // does not support boundary checking
        log(array.at(4));
    }
    catch (const std::exception& e)
    {
        log("Exception Thrown: "s + e.what());
    }

    // decltype(variable) equals to the type of "variable"
    decltype(array)::iterator iteratorBegin = array.begin();
    log("First element by iterator: "s + std::to_string(*iteratorBegin));
    // std::array<int, 3>::iterator is the same as decltype() call in previous line
    std::array<int, 3>::iterator iteratorEnd = array.end();
    log("Last element by iterator: "s + std::to_string(*(iteratorEnd - 1)));

    log("First element: "s + std::to_string(array.front()));
    log("Last element: "s + std::to_string(array.back()));

    log("Loop demos:");
    log(R"("Standard" loop:)");
    for (auto i = 0u; i < array.size(); ++i)
    {
        log(array[i]);
    }
    log(R"("Standard" loop using iterators:)");
    for (auto i = array.begin(); i != array.end(); ++i)
    {
        log(*i);
    }
    log("Range based loop:");
    for (const auto& a : array)
    {
        log(a);
    }
}

void runVectorDemo()
{ 
    log(std::string(__FUNCTION__) + "(): Demo start");
    std::vector<int> vector{1, 2, 3, 4}; // substitute for: int* dynArray = new int [5]{1, 2, 3, 4};
    std::stringstream ss;
    ss << "Capacity of new vector: " << vector.capacity() << " of size: " << vector.size();
    log(ss.str());
    ss.str(""s);

    vector.reserve(9);
    ss << "Capacity after reserving memory for 9 elements: " << vector.capacity() << " size: " << vector.size();
    log(ss.str());
    ss.str(""s);
    
    log("Adding a new element of value \"5\"");
    vector.push_back(5);
    ss << "capacity: " << vector.capacity() << " size: " << vector.size();
    log(ss.str());
    ss.str(""s);

    log("Adding five new elements");
    vector.push_back(6);
    vector.push_back(7);
    vector.push_back(8);
    vector.push_back(9);
    vector.push_back(10);

    ss << "capacity: " << vector.capacity() << " size: " << vector.size();
    log(ss.str());
    ss.str(""s);

    log("Shrinking capacity only for needed elements");
    vector.shrink_to_fit();

    ss << "capacity: " << vector.capacity() << " size: " << vector.size();
    log(ss.str());
    ss.str(""s);
}

void runMapDemo()
{
    log(std::string(__FUNCTION__) + "(): Demo start");
    std::map<double, std::string> map{{0.2, "a"}, {1.5, "b"}};
    log(map[0.2]);
    map[0.7] = "c";
    log(map[0.7]);
    log(map.at(1.5));
}

void runDemo()
{
    std::cout << std::endl;
    containers::runPairDemo();
    std::cout << std::endl;
    containers::runArrayDemo();
    std::cout << std::endl;
    containers::runVectorDemo();
    std::cout << std::endl;
    containers::runMapDemo();
    std::cout << std::endl;

    log("Other containers to check by yourself: ");
    log("std::list<int> l");
    log("std::forward_list<int> fl");
    log("std::deque<int> d");
    log("std::stack<int> s");
    log("std::set<int> set");
}

}

int main()
{
    containers::runDemo();
}