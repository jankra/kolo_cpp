# Nowoczesny C++

Repozytorium z kodem ze spotkań koła.

W katalogu *cpp_01* można znaleźc przykłady z typów wyliczeniowych, literałów, przeładowania operatorów, pętli for po zakresie oraz referencji.

W katalogu *cpp_02* można znaleźc ćwiczenia z kontenerów sekwencyjnych i lambd.

## Budowanie:

Kod można zbudować w dowolnie wybrany sposób, ale projekt jest skonfigurowany do automatycznego budowania.

W przypadku *cpp_02*, aby sprawdzić poprawność narzędz kompilacyjnych należy skompilować projekt cmake w katalogu 
*cpp_02/compilationTest* 

### Linux:

Aby skompilować kod, z poziomu katalogu *cpp_01* należy wywołać następujące komendy:

```cmake .```

a następnie:

```make```

Zostaną zbudowane cztery binarki, gdzie każda odpowiada odpowiednim demom.

Wymagane jest zainstalowanie *cmake* w wersji minimum 3.0.

Instalacja *cmake* w przypadku Ubuntu:

```
sudo apt-get install cmake
```

### Windows i Visual Studio:

[Odsyłam do poradnika w dokumentacji.](https://docs.microsoft.com/pl-pl/cpp/build/cmake-projects-in-visual-studio?view=vs-2019)

Z powodu różnic w implementacji niektórych funkcji w kompilatorze Microsoft Visual Compiler, nie gwarantuję, że przykład z literałami będzie działał w 100% -> kod znaleziony w przestrzeni nazw *darkmagic* nie był testowany na Windowsie.