#pragma once
#include <string>
#include <string>
#include <memory>
#include <cxxabi.h>
#include <iostream>

namespace darkMagic
{

#define PRINT_TYPE_NAME(arg) printWithTypeName(#arg, arg)

template <class T> 
std::string getTypeName()
{
    using DereferencedType = typename std::remove_reference<T>::type;
    std::unique_ptr<char, void (*)(void*)> own(
        abi::__cxa_demangle(typeid(DereferencedType).name(), nullptr, nullptr, nullptr), std::free);
    std::string typeName = own != nullptr ? own.get() : typeid(DereferencedType).name();
    if (std::is_const<DereferencedType>::value)
        typeName += " const";
    if (std::is_volatile<DereferencedType>::value)
        typeName += " volatile";
    if (std::is_lvalue_reference<T>::value)
        typeName += "&";
    else if (std::is_rvalue_reference<T>::value)
        typeName += "&&";
    return typeName;
}

template <typename T> void printWithTypeName(const std::string& arg, T input)
{
    std::cout << arg << " v: " << input << " t: " << getTypeName<decltype(input)>() << std::endl;
}
}