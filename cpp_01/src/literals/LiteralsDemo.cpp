#include <iostream>
#include <string>

#include "darkMagic/TypePrinter.hpp"

using namespace std::literals;

struct Century
{
    operator std::string()
    {
        return std::to_string(number) + " century"s;
    }
    
    unsigned long long number;
};

Century operator""_C (unsigned long long no)
{
    return Century{no};
}

int main()
{
    std::cout << "Integer type literals:" << std::endl;
    darkMagic::PRINT_TYPE_NAME(100);
    darkMagic::PRINT_TYPE_NAME(100u);
    darkMagic::PRINT_TYPE_NAME(100l);
    darkMagic::PRINT_TYPE_NAME(100lu);
    darkMagic::PRINT_TYPE_NAME(100ll);
    darkMagic::PRINT_TYPE_NAME(100llu);
    darkMagic::PRINT_TYPE_NAME(0xA);
    darkMagic::PRINT_TYPE_NAME(0b1010);
    darkMagic::PRINT_TYPE_NAME(1'000'000);

    std::cout << std::endl << "Floating type literals:" << std::endl;
    darkMagic::PRINT_TYPE_NAME(100.);
    darkMagic::PRINT_TYPE_NAME(100.l);
    darkMagic::PRINT_TYPE_NAME(100.f);
    darkMagic::PRINT_TYPE_NAME(10e2);
    darkMagic::PRINT_TYPE_NAME(10e-2);
    darkMagic::PRINT_TYPE_NAME(.32e-2f);
    darkMagic::PRINT_TYPE_NAME(.5e-2f);
    darkMagic::PRINT_TYPE_NAME(0x1.2p3);

    std::cout << std::endl << "Character literals:" << std::endl;
    darkMagic::PRINT_TYPE_NAME("100");
    darkMagic::PRINT_TYPE_NAME(u8"\U0001D11E");
    darkMagic::PRINT_TYPE_NAME(u8"\u0024");
    darkMagic::PRINT_TYPE_NAME(u8"\u2620");
    darkMagic::PRINT_TYPE_NAME(L"100");

    std::cout << std::endl << "String literals:" << std::endl;
    darkMagic::PRINT_TYPE_NAME("100"s);
    darkMagic::PRINT_TYPE_NAME(R"(Raw, unescaped string \n)");

    std::cout << std::endl << "User-Defined literals with user defined cast to std::string:" << std::endl;
    std::cout << static_cast<std::string>(21_C) << std::endl; 
}
