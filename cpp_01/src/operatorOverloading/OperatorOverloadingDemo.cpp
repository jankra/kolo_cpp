#include <string>
#include <iostream>

struct Complex
{
    int real;
    int imaginary;

    operator std::string()
    {
        return "real: " + std::to_string(real) + " imaginary: " + std::to_string(imaginary);
    }

    Complex operator+(const Complex& rhs)
    {
        return {real + rhs.real, imaginary + rhs.imaginary};
    }
};

Complex operator-(const Complex& lhs, const Complex& rhs)
{
    return {lhs.real - rhs.real, lhs.imaginary - rhs.imaginary};
}

int main()
{
    Complex a{2, 2};
    Complex b{1, 1};
    std::cout << "sum: " << static_cast<std::string>(a+b) << std::endl;
    std::cout << "difference: " << static_cast<std::string>(a-b) << std::endl;
}