#include <iostream>
#include <vector>
#include <array>

int main()
{
    int cStyleArray[5] = {1, 2, 3, 4, 5};
    std::cout << "C-Style Array, elements by copy:" << std::endl;
    for (int i : cStyleArray)
    {
        std::cout << i << std::endl;
    }

    int* cStyleDynamicArray = new int[5]{11, 22, 33, 44, 55};
    std::cout << "C-Style Dynamic Array, elements by ref:" << std::endl;
    for (int& i : cStyleArray)
    {
        std::cout << i << std::endl;
    }
    delete [] cStyleDynamicArray;

    std::vector<int> container {9, 8, 7, 6, 5};
    std::cout << "Sequential container, elements by deduced copy:" << std::endl;
    for (auto i : container)
    {
        std::cout << i << std::endl;
    }

    std::cout << "Anonymous initializer list, elements by deduced ref:" << std::endl;
    for (auto& c : {'a', 'b', 'c', 'd', 'e'})
    {
        std::cout << c << std::endl;
    }
}