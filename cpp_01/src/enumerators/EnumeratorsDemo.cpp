#include <iostream>
#include <string>

enum CStyleEnum
{
    Option0,
    Option1,
    Option2,
    Option12 = 12,
    Option13
};

enum DifferentTypeEnum : char
{
    CharA = 'a',
    CharB,
    CharC
};

enum class StronglyTypizedEnum
{
    Option0,
    Option1,
    Option2,
    Option12,
    Option13
};

void print(const std::string& text)
{
    std::cout << text << std::endl;
}

void printOption(int option)
{
    switch (option)
    {
    case Option0:
        print("C-Style Enum by int: Option0");
        break;
    case Option1:
        print("C-Style Enum by int: Option1");
        break;
    case Option2:
        print("C-Style Enum by int: Option2");
        break;
    case Option12:
        print("C-Style Enum by int: Option12");
        break;
    case Option13:
        print("C-Style Enum by int: Option13");
        break;
    default:
        print("Default");
    }
}

void printOption(CStyleEnum option)
{
    switch (option)
    {
    case Option0:
        print("C-Style Enum: Option0");
        break;
    case Option1:
        print("C-Style Enum: Option1");
        break;
    case Option2:
        print("C-Style Enum: Option2");
        break;
    case Option12:
        print("C-Style Enum: Option12");
        break;
    case Option13:
        print("C-Style Enum: Option13");
        break;
    default:
        print("Default");
    }
}

void printOption(StronglyTypizedEnum option)
{
    switch (option)
    {
    case StronglyTypizedEnum::Option0:
        print("Strongly Typized Enum: Option0");
        break;
    case StronglyTypizedEnum::Option1:
        print("Strongly Typized Enum: Option1");
        break;
    case StronglyTypizedEnum::Option2:
        print("Strongly Typized Enum: Option2");
        break;
    case StronglyTypizedEnum::Option12:
        print("Strongly Typized Enum: Option12");
        break;
    case StronglyTypizedEnum::Option13:
        print("Strongly Typized Enum: Option13");
        break;
    default:
        print("Default");
    }
}

void printChar(char character)
{

    switch (character)
    {
    case CharA:
        print("C-Style Enum with char type by char: Option0");
        break;
    case CharB:
        print("C-Style Enum with char type by char: Option1");
        break;
    case CharC:
        print("C-Style Enum with char type by char: Option2");
        break;
    default:
        print("Default");
    }
}

void printChar(DifferentTypeEnum character)
{

    switch (character)
    {
    case CharA:
        print("C-Style Enum with char type: Option0");
        break;
    case CharB:
        print("C-Style Enum with char type: Option1");
        break;
    case CharC:
        print("C-Style Enum with char type: Option2");
        break;
    default:
        print("Default");
    }
}

int main()
{
    printOption(1);
    printOption(13);
    printOption(Option2);
    printOption(StronglyTypizedEnum::Option1); // prefered option
    printChar('a');
    printChar(CharB);
}