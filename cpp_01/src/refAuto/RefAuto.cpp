#include <iostream>

int passByCopyAndIncrement(int a)
{
    return ++a;
}

int passByReferenceAndIncrement(int& a)
{
    return ++a;
}

int& returnReference(int& a)
{
    return a;
}

int main()
{
    int a = 5;
    std::cout << "original a: " << a 
              << ", function return: " << passByCopyAndIncrement(a) 
              << ", original a after function call: " << a << std::endl;
    std::cout << "original a: " << a 
              << ", function return: " << passByReferenceAndIncrement(a) 
              << ", original a after function call: " << a << std::endl
              << std::endl;

    int& referenceToA = a;
    std::cout << "a value: " << a
              << ", ref value: " << referenceToA << std::endl;
    ++referenceToA;
    std::cout << "After incrementing reference:" << std::endl;
    std::cout << "a value: " << a
              << ", ref value: " << referenceToA << std::endl
              << std::endl;


    auto copyOfObjectReferedFromFunction = returnReference(a);
    std::cout << "a value: " << a
              << ", copyOfObjectReferedFromFunction value: " << copyOfObjectReferedFromFunction << std::endl;
    ++copyOfObjectReferedFromFunction;
    std::cout << "After incrementing copyOfObjectReferedFromFunction:" << std::endl;
    std::cout << "a value: " << a
              << ", copyOfObjectReferedFromFunction value: " << copyOfObjectReferedFromFunction << std::endl
              << std::endl;

    auto& deducedReference = returnReference(a);
    std::cout << "a value: " << a
              << ", deducedReference value: " << deducedReference << std::endl;
    ++deducedReference;
    std::cout << "After incrementing deducedReference:" << std::endl;
    std::cout << "a value: " << a
              << ", deducedReference value: " << deducedReference << std::endl
              << std::endl;
}